<?php
namespace ZeroToHero\Traits;

trait DefinitionTrait
{
    /**
     * Class name (and namespace if exists)
     *
     * @return string
     */
    public function getClassName () : string
    {
        return $this->getDeclaringClass()->getName();
    }

    /**
     * Get the definition visibility
     *
     * @return string "public", "private" or "protected"
     */
    public function getVisibility () : string
    {
        if ( $this->isProtected() )
            return 'protected';
        elseif ( $this->isPrivate() )
            return 'private';
        else
            return 'public';
    }

    /**
     * Throw error with custom message (depending code given)
     *
     * @param string $name  Definition name with class (and namespace if exists)
     * @param object $error Error code (+ more info if necessary)
     *
     * @throws \ErrorException
     */
    public static function throwError ( string $name, object $error )
    {
        $code = $error->code;
        $more = $error->more ?? null;

        switch ( $code ) {
            case 'MUST_BE_DECLARED':
                $message = $name . ' must be declared';
                throw new \ErrorException( $message );

            case 'TYPE_MUST_BE_ARRAY':
                $message = $name . ' must be array';
                if ( $more )
                    $message .= ' (' . $more . ' given)';
                throw new \ErrorException( $message );
            case 'TYPE_MUST_BE_BOOL':
                $message = $name . ' must be boolean';
                if ( $more )
                    $message .= ' (' . $more . ' given)';
                throw new \ErrorException( $message );
            case 'TYPE_MUST_BE_DOUBLE':
                $message = $name . ' must be double';
                if ( $more )
                    $message .= ' (' . $more . ' given)';
                throw new \ErrorException( $message );
            case 'TYPE_MUST_BE_FLOAT':
                $message = $name . ' must be float';
                if ( $more )
                    $message .= ' (' . $more . ' given)';
                throw new \ErrorException( $message );
            case 'TYPE_MUST_BE_INT':
            case 'TYPE_MUST_BE_INTEGER':
                $message = $name . ' must be integer';
                if ( $more )
                    $message .= ' (' . $more . ' given)';
                throw new \ErrorException( $message );
            case 'TYPE_MUST_BE_NULL':
                $message = $name . ' must be null';
                if ( $more )
                    $message .= ' (' . $more . ' given)';
                throw new \ErrorException( $message );
            case 'TYPE_MUST_BE_STRING':
                $message = $name . ' must be string';
                if ( $more )
                    $message .= ' (' . $more . ' given)';
                throw new \ErrorException( $message );

            case 'VALUE_NOT_EQUALS':
                $message = $name . ' is not the value expected';
                if ( $more )
                    $message .= ' (' . $more . ')';
                throw new \ErrorException( $message );

            case 'VISIBILITY_MUST_BE_PUBLIC':
                $message = $name . ' must be public';
                if ( $more )
                    $message .= ' (' . $more . ' given)';
                throw new \ErrorException( $message );
            case 'VISIBILITY_MUST_BE_PRIVATE':
                $message = $name . ' must be private';
                if ( $more )
                    $message .= ' (' . $more . ' given)';
                throw new \ErrorException( $message );
            case 'VISIBILITY_MUST_BE_PROTECTED':
                $message = $name . ' must be protected';
                if ( $more )
                    $message .= ' (' . $more . ' given)';
                throw new \ErrorException( $message );
        }
    }

    /**
     * Check if the constant visibility is the same as the one given
     *
     * @param string $visibility "public", "private" or "protected"
     *
     * @throws \ErrorException
     */
    public function checkVisibility ( string $visibility )
    {
        $className      = $this->getClassName();
        $definitionName = $this->getName();
        $fullName       = self::getFullName( $className, $definitionName );

        if ( $visibility != $this->getVisibility() ) {
            $error       = new \stdClass;
            $error->code = 'VISIBILITY_MUST_BE_' . strtoupper( $visibility );
            $error->more = $this->getVisibility();
            $this->throwError( $fullName, $error );
        }
    }

    /**
     * Check if the constant type is the same as the one given
     *
     * @param string|null $typeName "string", "integer", "double", "array", etc.
     *
     * @throws \ErrorException
     */
    public function checkType ( string $typeName )
    {
        //if ( $typeName ) {
            $className      = $this->getClassName();
            $definitionName = $this->getName();
            $fullName       = self::getFullName( $className, $definitionName );

            if ( ! $this->checkTypeBool( $typeName ) ) {
                $type        = $this->getType();

                $error       = new \stdClass;
                $error->code = 'TYPE_MUST_BE_' . strtoupper( $typeName );
                $error->more = $type ? $type->getName() : 'null';

                $this->throwError( $fullName, $error );
            }
        //}
    }

    /**
     * Add double quotes for strings
     *
     * @param $value Value to check if double quotes are needed
     *
     * @return int|string
     */
    public static function formatString ( $value )
    {
        if ( ! is_numeric( $value ) )
            return sprintf( "\"%s\"", $value );
        else
            return $value;
    }

    /**
     * Check if the constant value is the same as the one given
     *
     * @param $value Whatever it can be
     *
     * @throws \ErrorException
     */
    public function equals ( $value )
    {
        $className      = $this->getClassName();
        $definitionName = $this->getName();
        $fullName       = self::getFullName( $className, $definitionName );

        if ( ! $this->equalsBool( $value ) ) {
            if ( is_bool( $value ) )
                $value = $value ? 'true' : 'false';
            elseif ( is_array( $value ) ) {
                $map   = array_map( 'formatString', $value );
                $value = '[' . implode( ',', $map ) . ']';
            }
            else
                $value = self::formatString( $value );

            $more        = $value . ' expected';
            $error       = new \stdClass;
            $error->code = 'VALUE_NOT_EQUALS';
            $error->more = $more;
            $this->throwError( $fullName, $error );
        }
    }

    /**
     * Check if the constant value is NOT the same as the one given
     *
     * @param $value Whatever it can be
     *
     * @throws \ErrorException
     */
    public function notEquals ( $value )
    {
        $className      = $this->getClassName();
        $definitionName = $this->getName();
        $fullName       = self::getFullName( $className, $definitionName );

        if ( $this->equalsBool( $value ) ) {
            if ( is_bool( $value ) )
                $value = $value ? 'true' : 'false';
            elseif ( is_array( $value ) ) {
                $map   = array_map( 'formatString', $value );
                $value = '[' . implode( ',', $map ) . ']';
            }
            else
                $value = self::formatString( $value );

            $more        = $value . ' given';
            $error       = new \stdClass;
            $error->code = 'VALUE_NOT_EQUALS';
            $error->more = $more;
            $this->throwError( $fullName, $error );
        }
    }

    /**
     * Check if the constant visibility is the same as the one given
     *
     * @param string $visibility "public", "private" or "protected"
     *
     * @return bool
     */
    public function checkVisibilityBool ( string $visibility ) : bool
    {
        if ( $visibility == 'null' && ! $this->isPublic() )
            return false;
        elseif ( $visibility == 'null' )
            return true;

        $checkVisibility = 'is' . ucfirst( $visibility );

        return $this->$checkVisibility();
    }

    /**
     * Check if the constant type is the same as the one given
     *
     * @param string $type "string", "int", "double", "array", etc.
     *
     * @return bool
     */
    public function checkTypeBool ( string $type ) : bool
    {
        if ( get_class( $this ) == 'ZeroToHero\PropertyScan' ) {
            $className = $this->getClassName();
            $class     = new $className;
            $typeName  = strtolower( gettype( $this->getValue( $class ) ) );
            $replace   = [
                'bool'  => 'boolean',
                'float' => 'double',
                'int'   => 'integer',
            ];
            foreach ( $replace as $key => $val )
                if ( $type == $key )
                    $type = $val;
        }
        else
            $typeName = strtolower( gettype( $this->getValue() ) );

        if ( $type != $typeName )
            return false;

        return true;
    }

    /**
     * Check if the constant value is the same as the one given
     *
     * @param $value Whatever it can be
     *
     * @return bool
     */
    public function equalsBool ( $value ) : bool
    {
        if ( get_class( $this ) == 'ZeroToHero\PropertyScan' ) {
            $className = $this->getClassName();
            $class     = new $className;
            $typeName  = strtolower( gettype( $this->getValue( $class ) ) );

            return $this->getValue( $class ) === $value;
        }
        else
            return $this->getValue() === $value;
    }
}