<?php
namespace ZeroToHero;

use ZeroToHero\Interfaces\DefinitionInterface;
use ZeroToHero\Traits\DefinitionTrait;

/**
 * This class extends ReflectionClassConstant. It can then report custom
 * informations and error messages.
 *
 * @author  Julian EDELIN <julian@edelin.fr>
 * @package ZeroToHero
 */
class ConstantScan
    extends \ReflectionClassConstant
    implements DefinitionInterface
{
    use DefinitionTrait;

    /**
     * ConstantClass constructor
     *
     * @param object $object   Object that contains the constant
     * @param string $constant Constant name
     */
    public function __construct ( object $object, string $constant )
    {
        parent::__construct( $object, $constant );
    }

    /**
     * Constant name with class (and namespace if exists)
     *
     * @param string $class Class name
     * @param string $name  Constant name
     *
     * @return string
     */
    public static function getFullName ( string $class, string $name ) : string
    {
        return $class . '::' . $name;
    }
}