<?php
namespace ZeroToHero\Interfaces;

interface DefinitionInterface
{
    public function __construct ( object $object, string $constant );

    public function getClassName () : string;

    public static function getFullName ( string $class, string $name ) : string;

    public static function throwError ( string $name, object $error );

    public function checkVisibility ( string $visibility );

    public function checkType ( string $type );

    public function equals ( $value );

    public function notEquals ( $value );

    public function checkVisibilityBool ( string $visibility ) : bool;

    public function checkTypeBool ( string $type ) : bool;

    public function equalsBool ( $value ) : bool;
}