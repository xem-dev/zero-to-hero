<?php
namespace ZeroToHero;

use ZeroToHero\Interfaces\DefinitionInterface;
use ZeroToHero\Traits\DefinitionTrait;

/**
 * This class extends ReflectionProperty. It can then report custom
 * informations and error messages.
 *
 * @author  Julian EDELIN <julian@edelin.fr>
 * @package ZeroToHero
 */
class PropertyScan
    extends \ReflectionProperty
    implements DefinitionInterface
{
    use DefinitionTrait;

    /**
     * PropertyScan constructor
     *
     * @param object $object   Object that contains the property
     * @param string $property Property name
     *
     * @throws \ReflectionException
     */
    public function __construct ( object $object, string $property )
    {
        parent::__construct( $object, $property );
        $this->setAccessible( true );
    }

    /**
     * Property name with class (and namespace if exists)
     *
     * @param string $class Class name
     * @param string $name  Constant name
     *
     * @return string
     */
    public static function getFullName ( string $class, string $name ) : string
    {
        return $class . '::$' . $name;
    }
}