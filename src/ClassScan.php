<?php
namespace ZeroToHero;

/**
 * This class extends ReflectionClass and recovers all constants, properties
 * and methods of a given object. It can then report custom informations and
 * error messages.
 *
 * @author  Julian EDELIN <julian@edelin.fr>
 * @package ZeroToHero
 */
class ClassScan
    extends \ReflectionClass
{
    private array $constantsScan  = [];
    private array $propertiesScan = [];
    private array $methodsScan    = [];

    /**
     * ConstantScan global getter
     *
     * @return array Array of ConstantScan
     */
    public function getConstantsScan () : array
    {
        return $this->constantsScan;
    }

    /**
     * PropertyScan global getter
     *
     * @return array Array of PropertyScan
     */
    public function getPropertiesScan () : array
    {
        return $this->propertiesScan;
    }

    /**
     * MethodScan global getter
     *
     * @return array Array of MethodScan
     */
    public function getMethodsScan () : array
    {
        return $this->methodsScan;
    }

    /**
     * ConstantScan specific getter
     *
     * @param string $name Constant name
     *
     * @return \ZeroToHero\ConstantScan
     * @throws \ErrorException
     */
    public function getConstantScan ( string $name ) : ConstantScan
    {
        $constantsScan = $this->getConstantsScan();

        if ( ! in_array( $name, array_keys( $constantsScan ) ) ) {
            $fullName = ConstantScan::getFullName( $this->getName(), $name );
            $error = new \stdClass;
            $error->code = 'MUST_BE_DECLARED';
            ConstantScan::throwError( $fullName, $error );
        }

        return $constantsScan[ $name ];
    }

    /**
     * PropertyScan specific getter
     *
     * @param string $name Property name
     *
     * @return \ZeroToHero\PropertyScan
     * @throws \ErrorException
     */
    public function getPropertyScan ( string $name ) : PropertyScan
    {
        $propertiesScan = $this->getPropertiesScan();

        if ( ! in_array( $name, array_keys( $propertiesScan ) ) ) {
            $fullName = PropertyScan::getFullName( $this->getName(), $name );
            $error = new \stdClass;
            $error->code = 'MUST_BE_DECLARED';
            ConstantScan::throwError( $fullName, $error );
        }

        return $propertiesScan[ $name ];
    }

    /**
     * MethodScan specific getter
     *
     * @param string $name Method name
     *
     * @return \ZeroToHero\Models\MethodScan
     * @throws \ErrorException
     */
    public function getMethodScan ( string $name ) : MethodScan
    {
        $methodsScan = $this->getMethodsScan();

        if ( ! in_array( $name, array_keys( $methodsScan ) ) ) {
            $fullName = ConstantScan::getFullName( $this->getName(), $name );
            ConstantScan::throwError( 'MUST_BE_DECLARED', $fullName );
        }

        return $methodsScan[ $name ];
    }

    /**
     * ConstantScan specific setter
     *
     * @param \ReflectionClassConstant $object Constant object
     *
     * @return \ZeroToHero\Models\ClassScan
     */
    public function addConstant ( \ReflectionClassConstant $object ) : ClassScan
    {
        $className    = $this->getName();
        $class        = new $className;
        $constantScan = new ConstantScan( $class, $object->getName() );

        $constantName                         = $constantScan->getName();
        $this->constantsScan[ $constantName ] = $constantScan;

        return $this;
    }

    /**
     * PropertyScan specific setter
     *
     * @param \ReflectionProperty $object Constant object
     *
     * @return \ZeroToHero\Models\ClassScan
     * @throws \ReflectionException
     */
    public function addProperty ( \ReflectionProperty $object ) : ClassScan
    {
        $className    = $this->getName();
        $class        = new $className;
        $propertyScan = new PropertyScan( $class, $object->getName() );

        $propertyName                          = $propertyScan->getName();
        $this->propertiesScan[ $propertyName ] = $propertyScan;

        return $this;
    }

    /**
     * PropertyScan specific setter
     *
     * @param \ReflectionMethod $object Constant object
     *
     * @return \ZeroToHero\Models\ClassScan
     * @throws \ReflectionException
     */
    public function addMethod ( \ReflectionMethod $object ) : ClassScan
    {
        $className  = $this->getName();
        $class      = new $className;
        $methodScan = new PropertyScan( $class, $object->getName() );

        $methodName                       = $methodScan->getName();
        $this->methodsScan[ $methodName ] = $methodScan;

        return $this;
    }

    /**
     * ClassScan constructor
     *
     * @param string $class Object name
     *
     * @throws \ReflectionException
     */
    public function __construct ( string $class )
    {
        parent::__construct( $class );

        $this->fillDefinitions();
    }

    /**
     * Fill ConstantScan, PropertyScan and MethodScan arrays
     *
     * @return \ZeroToHero\Models\ClassScan
     * @throws \ReflectionException
     */
    public function fillDefinitions () : ClassScan
    {
        foreach ( $this->getReflectionConstants() as $constant )
            $this->addConstant( $constant );
        foreach ( $this->getProperties() as $property )
            $this->addProperty( $property );
        foreach ( $this->getMethods() as $method )
            $this->addMethod( $method );

        return $this;
    }
}