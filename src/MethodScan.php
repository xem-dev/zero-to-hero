<?php
namespace ZeroToHero;

use ZeroToHero\Interfaces\DefinitionInterface;
use ZeroToHero\Traits\DefinitionTrait;

/**
 * This class extends ReflectionMethod. It can then report custom
 * informations and error messages.
 *
 * @author  Julian EDELIN <julian@edelin.fr>
 * @package ZeroToHero
 */
class MethodScan
    extends \ReflectionMethod
    implements DefinitionInterface
{
    use DefinitionTrait;
    /**
     * MethodScan constructor.
     *
     * @param object $object Object that contains the method
     * @param string $method Method name
     *
     * @throws \ReflectionException
     */
    public function __construct ( object $object, string $method )
    {
        parent::__construct( $object, $method );
    }

    /**
     * Constant name with class (and namespace if exists)
     *
     * @param string $class Class name
     * @param string $name  Constant name
     *
     * @return string
     */
    public static function getFullName ( string $class, string $name ) : string
    {
        return $class . '::' . $name . '()';
    }
}