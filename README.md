# Zero To Hero

## Description

This project is to learn, review and/or deepen what PHP can do with its OOP
(Object-Oriented Programming) format.

## Installation

Clone the project. Once done, use the dependency manager
[Composer](https://getcomposer.org/doc/00-intro.md) to install the
autoloading.

```
composer install
```

## Autoload

After the installation and each `git pull`, the autoloading might need to be
updated.

```
composer dump-autoload -o
```

## How does the project work ?

All exercises are in the `exercises/` folder.

```
# Tree structure example

exercises
|-- exercise0
    |-- src
    |   |-- index.php
    |-- check.php
```

All the work have to be in the `src/` folder, mainly on its `index.php` file.
Everything outside it doesn't need to be opened, since it's just for checking.

The goal is to reach the "Congratulations!" message.