<?php
/**
 * Exercise #1
 * Constants and properties
 *
 * @author Julian Edelin <julian@edelin.fr>
 *
 * Create "NewClass" class:
 * - with a "A" protected constant with the value 0.01
 * - with a "B" public constant with the value 0
 * - with a "C" private constant with the values "even" and "odd"
 * - with a "D" protected constant which is false
 * - with a "E" private constant with the value "Hello!"
 * - with a "F" public constant which is null
 * - with a "a" public property with the value "even" and "odd" by default
 * - with a "b" private property which is true by default
 * - with a "c" protected property with the value 3.14 by default
 * - with a "d" public property with the value 25 by default
 * - with a "e" private property which is null by default
 * - with a "f" protected property with the value "World!" by default
 * - with a "g" public property which can be anything and no value by default
 */