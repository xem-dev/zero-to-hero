<?php
/**
 * Autoload classes
 */
require_once( '../../vendor/autoload.php' );

use ZeroToHero\ClassScan;

/**
 * Exercise code recovery
 */
require_once( 'src/index.php' );

/**
 * Class check
 */
$classScan = new ClassScan( 'NewClass' );

/**
 * Constants check
 */
$constants = [
    new class {
        public string $name       = 'A';
        public string $visibility = 'protected';
        public string $type       = 'double';
        public        $value      = 0.01;
    },
    new class {
        public string $name       = 'B';
        public string $visibility = 'public';
        public string $type       = 'integer';
        public        $value      = 0;
    },
    new class {
        public string $name       = 'C';
        public string $visibility = 'private';
        public string $type       = 'array';
        public        $value      = [ 'even', 'odd' ];
    },
    new class {
        public string $name       = 'D';
        public string $visibility = 'protected';
        public string $type       = 'boolean';
        public        $value      = false;
    },
    new class {
        public string $name       = 'E';
        public string $visibility = 'private';
        public string $type       = 'string';
        public        $value      = 'Hello!';
    },
    new class {
        public string $name       = 'F';
        public string $visibility = 'public';
        public string $type       = 'null';
    },
];
foreach ( $constants as $constant ) {
    $constantScan = $classScan->getConstantScan( $constant->name );
    $constantScan->checkVisibility( $constant->visibility );
    $constantScan->checkType( $constant->type ?? 'null' );
    $constantScan->equals( $constant->value ?? null );
}

/**
 * Properties check
 */
$properties = [
    new class {
        public string $name       = 'a';
        public string $visibility = 'public';
        public string $type       = 'array';
        public        $value      = [ 'even', 'odd' ];
    },
    new class {
        public string $name       = 'b';
        public string $visibility = 'private';
        public string $type       = 'bool';
        public        $value      = true;
    },
    new class {
        public string $name       = 'c';
        public string $visibility = 'protected';
        public string $type       = 'float';
        public        $value      = 3.14;
    },
    new class {
        public string $name       = 'd';
        public string $visibility = 'public';
        public string $type       = 'int';
        public        $value      = 25;
    },
    new class {
        public string $name       = 'e';
        public string $visibility = 'private';
        public string $type       = 'null';
    },
    new class {
        public string $name       = 'f';
        public string $visibility = 'public';
        public string $type       = 'string';
        public        $value      = 'World!';
    },
    new class {
        public string $name       = 'g';
        public string $visibility = 'public';
    },
];
foreach ( $properties as $property ) {
    $propertyScan = $classScan->getPropertyScan( $property->name );
    $propertyScan->checkVisibility( $property->visibility );
    $propertyScan->checkType( $property->type ?? 'null' );
    $propertyScan->equals( $property->value ?? null );
}

/**
 * Congratulations!
 */
echo 'Congratulations! Everything has been reached! Exercise #1 done!';